##############################################################################
# CMake
##############################################################################

cmake_minimum_required(VERSION 2.8.8)
project(gs_gui)
#set(QT_QMAKE_EXECUTABLE /usr/bin/qmake-qt5)

##############################################################################
# Catkin
##############################################################################


# qt_build provides the qt cmake glue, roscpp the comms for a default talker
find_package(catkin REQUIRED COMPONENTS roscpp ardrone_autonomy sensor_msgs cv_bridge)
include_directories(${catkin_INCLUDE_DIRS} ${PROJECT_SOURCE_DIR}/include/gs_gui)
# Use this to define what the package will export (e.g. libs, headers).
# Since the default here is to produce only a binary, we don't worry about
# exporting anything. 
catkin_package(
    DEPENDS roscpp std_msgs ardrone_autonomy sensor_msgs opencv2 cv_bridge
)

#set(CMAKE_CXX_FLAGS "-fPIC")
##############################################################################
# Qt Environment
##############################################################################

# this comes from qt_build's qt-ros.cmake which is automatically 
# included via the dependency call in manifest.xml
#rosbuild_prepare_qt4_my(QtCore QtGui) # Add the appropriate components to the component list here

# Tell CMake to run moc when necessary:
set(CMAKE_AUTOMOC ON)
# As moc files are generated in the binary dir, tell CMake
# to always look for includes there:
set(CMAKE_INCLUDE_CURRENT_DIR ON)

# Widgets finds its own dependencies.
find_package(Qt5Widgets REQUIRED)

##############################################################################
# Sections
##############################################################################

file(GLOB QT_FORMS RELATIVE ${CMAKE_CURRENT_SOURCE_DIR} ui/*.ui)
file(GLOB QT_RESOURCES RELATIVE ${CMAKE_CURRENT_SOURCE_DIR} resources/*.qrc)
file(GLOB_RECURSE QT_MOC RELATIVE ${CMAKE_CURRENT_SOURCE_DIR} FOLLOW_SYMLINKS include/gs_gui/*.hpp include/gs_gui/*.h)

QT5_ADD_RESOURCES(QT_RESOURCES_CPP ${QT_RESOURCES})
QT5_WRAP_UI(QT_FORMS_HPP ${QT_FORMS})
QT5_WRAP_CPP(QT_MOC_HPP ${QT_MOC})

##############################################################################
# Sources
##############################################################################

file(GLOB_RECURSE QT_SOURCES RELATIVE ${CMAKE_CURRENT_SOURCE_DIR} FOLLOW_SYMLINKS src/*.cpp)

##############################################################################
# Binaries
##############################################################################

add_executable(gs_gui ${QT_SOURCES} ${QT_RESOURCES_CPP} ${QT_FORMS_HPP} ${QT_MOC_HPP})

target_link_libraries(gs_gui ${QT_LIBRARIES} ${catkin_LIBRARIES} Qt5::Widgets)
install(TARGETS gs_gui RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION})

