/**
 * @file /src/qnode.cpp
 *
 * @brief Ros communication central!
 *
 * @date February 2011
 **/

/*****************************************************************************
** Includes
*****************************************************************************/

#include <ros/ros.h>
#include <ros/network.h>
#include <string>
#include <std_msgs/Empty.h>
#include <sstream>
#include "../include/gs_gui/qnode.hpp"


#include <QMessageBox>

/*****************************************************************************
** Namespaces
*****************************************************************************/

namespace gs_gui {

/*****************************************************************************
** Implementation
*****************************************************************************/

QNode::QNode(int argc, char** argv ) :
    init_argc(argc), init_argv(argv), countNavMagMsg(0), countNavMsg(0), countPoseMsg(0)
	{}

QNode::~QNode() {
    if(ros::isStarted()) {
      ros::shutdown(); // explicitly needed since we use ros::start();
      ros::waitForShutdown();
    }
    wait();
}

bool QNode::init() {
	ros::init(init_argc,init_argv,"gs_gui");
	if ( ! ros::master::check() ) {
		return false;
	}
	ros::start(); // explicitly needed since our nodehandle is going out of scope.
	ros::NodeHandle n;
    ROS_INFO("Init gs_gui");
	// Add your ros communications here.
    start_publisher = n.advertise<std_msgs::Empty>("/start", 1);
    land_publisher = n.advertise<std_msgs::Empty>("/ardrone/land", 1);
    reset_publisher = n.advertise<std_msgs::Empty>("/ardrone/reset", 1);
    manualControl_publisher = n.advertise<geometry_msgs::Twist>("/manual_control", 1);
    autoControl_publisher = n.advertise<std_msgs::Empty>("/auto_control", 1);
    ranges_sub = n.subscribe("ardrone/ranges", 1, &QNode::cbRanges, this); // USB Data
    navi_sub = n.subscribe("/ardrone/navdata", 1, &QNode::cbNavi, this);
    navi_mag_sub = n.subscribe("/ardrone/navdata_magneto", 1, &QNode::cbNaviMag, this);
    pose_sub = n.subscribe("/pose",1, &QNode::cbPose,this);
    image_transport::ImageTransport it(n);
    camera_sub = it.subscribe("/result_image", 1, &QNode::cbCamera, this); // прописать топик с коптера ardrone/image_raw /bottom/bottom_camera/image_raw
    start();
    return true;
}

void QNode::sendStartMsg()
{
    ROS_INFO("Start");
    std_msgs::Empty msg;
    start_publisher.publish(msg);
}

void QNode::cbRanges(ardrone_autonomy::Ranges msg)
{
    Q_EMIT rangesChanged(msg.range_front,msg.range_left,msg.range_right);
}

void QNode::cbNavi(ardrone_autonomy::Navdata msg)
{
    if (countNavMsg>10){
        Q_EMIT navDataChanged(msg);
        countNavMsg=0;
    }
    countNavMsg++;
}

void QNode::cbNaviMag(ardrone_autonomy::navdata_magneto msg)
{
    if (countNavMagMsg>10){
        Q_EMIT navDataMagChanged(msg);
        countNavMagMsg=0;
    }
    countNavMagMsg++;

}

void QNode::cbCamera(const sensor_msgs::ImageConstPtr &msg)
{
    Q_EMIT cameraImageChanged(msg);
}

void QNode::cbPose(geometry_msgs::PoseStamped msg)
{
    Q_EMIT poseChanged(msg);
}

void QNode::sendLandMsg()
{
    std_msgs::Empty msg;
    land_publisher.publish(msg);
}

void QNode::sendResetMsg()
{
    std_msgs::Empty msg;
    reset_publisher.publish(msg);
}

void QNode::sendAutoControlMsg()
{
    std_msgs::Empty msg;
    autoControl_publisher.publish(msg);
}

void QNode::sendManualControl(geometry_msgs::Twist msg)
{
    manualControl_publisher.publish(msg);
}

void QNode::run()
{
    ros::spin();
}


}  // namespace gs_gui
