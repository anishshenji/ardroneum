/**
 * @file /include/gs_gui/main_window.hpp
 *
 * @brief Qt based gui for gs_gui.
 *
 * @date November 2010
 **/
#ifndef gs_gui_MAIN_WINDOW_H
#define gs_gui_MAIN_WINDOW_H

/*****************************************************************************
** Includes
*****************************************************************************/

#include <QtWidgets/QMainWindow>
#include "ui_main_window.h"
#include "qnode.hpp"
#include <ardrone_autonomy/Navdata.h>
#include <ardrone_autonomy/navdata_magneto.h>
#include <opencv2/core/core.hpp>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Twist.h>

/*****************************************************************************
** Namespace
*****************************************************************************/

namespace gs_gui {

/*****************************************************************************
** Interface [MainWindow]
*****************************************************************************/
/**
 * @brief Qt central, all operations relating to the view part here.
 */
class MainWindow : public QMainWindow {
Q_OBJECT

public:
	MainWindow(int argc, char** argv, QWidget *parent = 0);
	~MainWindow();

    void showNoMasterMessage();
    void keyPressEvent(QKeyEvent *event);

public Q_SLOTS:
    /******************************************
    ** Auto-connections (connectSlotsByName())
    *******************************************/
    void on_actionAbout_triggered();
    void on_button_Start_clicked();
    void on_button_Land_clicked();
    void on_button_Reset_clicked();

    void rangesChanged(double front, double left, double right);
    void navDataChanged(ardrone_autonomy::Navdata msg);
    void navDataMagnetoChanged(ardrone_autonomy::navdata_magneto msg);
    void cameraImageChanged(const sensor_msgs::ImageConstPtr &msg);
    void poseChanged(geometry_msgs::PoseStamped msg);

private:
	Ui::MainWindowDesign ui;
	QNode qnode;
    cv::Mat conversion_mat_;
};

}  // namespace gs_gui

#endif // gs_gui_MAIN_WINDOW_H
