#include <ros/ros.h>
#include <sensor_msgs/Range.h>
#include <ardrone_autonomy/Ranges.h>

ros::Publisher ranges_pub;
double forward, left, right;

void PublishRanges()
{
  ardrone_autonomy::Ranges msg;
  msg.range_front = forward*100;
  msg.range_left = left*100;
  msg.range_right = right*100;
  ranges_pub.publish(msg);
}

void rangeCallback1(sensor_msgs::Range const &msg) {
  forward = msg.range;
  PublishRanges();
}

void rangeCallback2(sensor_msgs::Range const &msg) {
  left = msg.range;
  PublishRanges();
}

void rangeCallback3(sensor_msgs::Range const &msg) {
  right = msg.range;
  PublishRanges();
}



int main(int argc, char** argv) {
  ros::init(argc, argv, "range_to_ranges");

  ros::NodeHandle priv_nh("~");

  ros::NodeHandle node;
  ros::Subscriber sub1, sub2, sub3;
  sub1 = node.subscribe("sonar_forward", 10, &rangeCallback1);
  sub2 = node.subscribe("sonar_left", 10, &rangeCallback2);
  sub3 = node.subscribe("sonar_right", 10, &rangeCallback3);
  ranges_pub = node.advertise<ardrone_autonomy::Ranges>("ardrone/ranges", 1);

  ros::spin();

  return 0;
}
