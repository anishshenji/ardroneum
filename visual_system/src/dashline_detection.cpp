#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/core/core.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <string>
#include <std_msgs/Empty.h>
#include <visual_system/Dashline.h>
#include <visual_system/DebugDashLines.h>
#include <ardrone_autonomy/navdata_magneto.h>

using namespace std;

using namespace cv;

namespace enc = sensor_msgs::image_encodings;

static const char WINDOW[] = "Image window";

class Dash
{
public:
	std::vector<cv::Point2f> points;
	cv::Point2f center;
	double slope;
	int idGroup;
	int indMaxside;
	bool isDelete;

	Dash()
	{
		idGroup=0;
		indMaxside=-1;
		isDelete = false;
	}

	void CalculateParameters()
	{
		//calculation center of dash and slope of maximal side

		float sum_x,sum_y;
		sum_x=0;
		sum_y=0;

		int n = points.size();

		float maxDist = 0;

		for(size_t i=0; i<n; ++i)
		{
			sum_x+=points[i].x;
			sum_y+=points[i].y;
			cv::Point side = points[i] - points[(i+1)%4];
			float squaredSideLength = side.dot(side);
			if (squaredSideLength>maxDist)
			{
				maxDist = squaredSideLength;
				indMaxside = i;
			}
		}
		center.x = sum_x/n;
		center.y = sum_y/n;
		float dx = points[(indMaxside+1)%4].x - points[indMaxside].x;
		if (dx!=0)
			slope = (points[(indMaxside+1)%4].y - points[indMaxside].y)/(dx);
		else
			slope = 9999999;

	}

	void Draw(cv::Mat& Img)
	{
		int thickness;
		cv::Scalar color;
		switch (idGroup)
		{
			case 0:
				color = CV_RGB(240, 0 ,0);
				break;
			case 1:
				color = CV_RGB(0, 240 ,0);
				break;
			case 2:
				color = CV_RGB(0, 0 , 240);
				break;
			default:
				color = CV_RGB(240, 0 , 240);
				break;
		}
		for (size_t j = 0; j<4; j++){
			if (j==indMaxside){
				thickness = 5;
			}
			else{
				thickness = 1;
			}
			cv::line(Img, points[j], points[(j+1)%4], color, thickness);
		}

		cv::circle(Img, center, 3, color);
	}

};

class DashLineDetect
{
	ros::NodeHandle nh_;
	ros::Publisher line_pub;
	ros::Publisher debug_ds_pub;
    ros::Subscriber navi_mag_sub;
	image_transport::ImageTransport it_;
	image_transport::Subscriber image_sub_;
	image_transport::Publisher filter_image_pub_;
	image_transport::Publisher threshold_image_pub_;
	image_transport::Publisher result_image_pub_;
	string  video_filename;
	string image_topic;
	cv::CascadeClassifier cascade_;
	cv::Mat cv_image_in;
	cv::Mat cv_image_out_;
	cv::Mat cv_image_gray_;  /**< Grayscale image */
	cv::Mat cv_thresholdImg_;
	double contrast_gain, gauss_sigma, brightness;
	int min_contour_size, minContourLength,threshold_block_size, threshold_constant ;
	std::vector<std::vector<cv::Point> > contours;
	std::vector<Dash> dashs;
	int indVertDash;
    double lastDirection;

public:
	DashLineDetect()
		: it_(nh_)
	{

		//load parameters
		bool use_video = ros::param::get("~video_filename", video_filename);

		ros::param::get("~image_topic", image_topic);

		contrast_gain = (ros::param::get("~contrast_gain", contrast_gain)) ? contrast_gain : 1.0;

		brightness = (ros::param::get("~brightness", brightness)) ? brightness : 0;
		min_contour_size = (ros::param::get("~min_contour_size", min_contour_size )) ? min_contour_size  : 40;
		minContourLength = (ros::param::get("~minContourLength", minContourLength )) ? minContourLength  : 20;

		threshold_block_size = (ros::param::get("~threshold_block_size", threshold_block_size)) ? threshold_block_size : 7;

		threshold_constant = (ros::param::get("~threshold_constant", threshold_constant)) ? threshold_constant : 7;

		line_pub = nh_.advertise<visual_system::Dashline>("/dashline", 1);
		debug_ds_pub = nh_.advertise<visual_system::DebugDashLines>("/debug_dashlines", 1);

        navi_mag_sub = nh_.subscribe("ardrone/navdata_magneto", 1, &DashLineDetect::navi_magCb, this);
		
		filter_image_pub_=it_.advertise("/filter_image", 1);
		threshold_image_pub_=it_.advertise("/threshold_image", 1);
		result_image_pub_=it_.advertise("/result_image", 1);

        //cv::namedWindow("Dashline detector: Dashline Detection", CV_WINDOW_AUTOSIZE);

		indVertDash = -1;

        lastDirection = 0.0;

		if (use_video)
		{
			CaptureVideoFile();
		} else
		{
			image_sub_ = it_.subscribe(image_topic, 1, &DashLineDetect::imageCb, this);// прописать топик с коптера ardrone/image_raw /bottom/bottom_camera/image_raw
		}


	}

	~DashLineDetect()
	{
		cv::destroyWindow(WINDOW);
	}

    void navi_magCb(ardrone_autonomy::navdata_magneto msg)
    {
        lastDirection = msg.heading_gyro_unwrapped;
    }

	void PublishDashlines()
	{
		visual_system::DebugDashLines msg;
		ros::Time time = ros::Time::now();
		msg.header.stamp = time;

		msg.count = dashs.size();

		msg.angles = vector<float>(msg.count);
		msg.xc = vector<float>(msg.count);
		msg.yc = vector<float>(msg.count);
		msg.idgroup = vector<int>(msg.count);

		for (size_t i=0; i<dashs.size(); i++)
		{
			msg.angles[i] = atan(dashs[i].slope);
			msg.xc[i] = dashs[i].center.x;
			msg.yc[i] = dashs[i].center.y;
			msg.idgroup[i] = dashs[i].idGroup;
		}

		debug_ds_pub.publish(msg);


	}

	void PublishImage(image_transport::Publisher &image_pub_, cv::Mat &image_, string encoding)
	{
		ros::Time time = ros::Time::now();
		cv_bridge::CvImagePtr cv_ptr(new cv_bridge::CvImage);
		cv_ptr->encoding = encoding;
		cv_ptr->header.stamp = time;
		cv_ptr->header.frame_id = "/base_link";
		cv_ptr->image = image_;
		image_pub_.publish(cv_ptr->toImageMsg());
	}

	void FilterAndThresholdImage()
	{

		float kernel[9];
		kernel[0]=-0.1;
		kernel[1]=-0.1;
		kernel[2]=-0.1;

		kernel[3]=-0.1;
		kernel[4]=2;
		kernel[5]=-0.1;

		kernel[6]=-0.1;
		kernel[7]=-0.1;
		kernel[8]=-0.1;

		// матрица
		cv::Mat kernel_matrix=cv::Mat(3,3,CV_32FC1,kernel);

		// накладываем фильтр
        //cv::filter2D(cv_image_in, cv_image_in, -1, kernel_matrix, cvPoint(-1,-1));
		
		

		if (cv_image_in.channels() == 1) {
			cv_image_gray_.create(cv_image_in.size(), CV_8UC1);
			cv_image_in.copyTo(cv_image_gray_);
		}
		else if (cv_image_in.channels() == 3) {
			cv_image_gray_.create(cv_image_in.size(), CV_8UC1);
			cv::cvtColor(cv_image_in, cv_image_gray_, CV_BGR2GRAY);
		}
		else {
			std::cerr << "Unknown image type"<<std::endl;
		}


		cv_image_gray_.convertTo(cv_image_gray_,-1, contrast_gain , brightness);
		

		PublishImage(filter_image_pub_, cv_image_gray_, "mono8");

		
        cv::adaptiveThreshold(cv_image_gray_,
                // Input image
                cv_thresholdImg_,// Result binary image
                255,
                //
                cv::ADAPTIVE_THRESH_GAUSSIAN_C, //
                cv::THRESH_BINARY_INV, //
                threshold_block_size, //
                threshold_constant //
        );

        //cv::Canny(cv_image_gray_,cv_thresholdImg_,10,200);

		PublishImage(threshold_image_pub_, cv_thresholdImg_, "mono8");

	}

    bool checkStraightPath()
    {
        int n = dashs.size();
        //found the max differents of slope of dashes from first group
        double maxDiff = -1;
        for (int i=0; i<n; i++)
        {
            for (int j=0; j<n; j++)
                if (i!=j && dashs[i].idGroup==1 && dashs[j].idGroup==1)
                {
                    double Diff = fabs(fabs(atan(dashs[i].slope)) - fabs(atan(dashs[j].slope)));
                    if (Diff>maxDiff)
                        maxDiff=Diff;
                }
        }
        if (maxDiff>0 && maxDiff<0.1)
            return true;
        return false;
    }

	void imageCb(const sensor_msgs::ImageConstPtr& msg)
	{

		cv_bridge::CvImageConstPtr image_ptr = cv_bridge::toCvShare(msg,sensor_msgs::image_encodings::BGR8);
		cv_image_in = image_ptr->image;
		
		
		FilterAndThresholdImage();


		findContours(min_contour_size);
		//drawAllContours(image);
		findDashes(minContourLength);
		GroupDashesBySlope();

		PublishDashlines();

		drawAllDashes(cv_image_in);
		double slope, intercept, dist;
		CalculateLine(slope, intercept);

        cv::Point2d center(cv_thresholdImg_.size().width/2, cv_thresholdImg_.size().height*3/4);

		dist=CalculateDistToLine(slope, intercept, center);

		visual_system::Dashline line_msg;
		line_msg.header.stamp = ros::Time::now();
		line_msg.slope = slope;
		line_msg.distance = dist;
        line_msg.straightPath = checkStraightPath();

		line_pub.publish(line_msg);

		int fontFace = FONT_ITALIC;
		double fontScale = 1;
		int thickness = 2;

		std::string textSlope = boost::lexical_cast<std::string>(slope);
		std::string textIntercept = boost::lexical_cast<std::string>(intercept);
		std::string text = "Line y="+textSlope+"*x+"+textIntercept;

		std::stringstream ss;
		ss<<"Line y="  << boost::lexical_cast<std::string>(slope).substr(0,6)<<"*x+" << boost::lexical_cast<std::string>(textIntercept).substr(0,6);
		text = ss.str();

		Point textOrg(10,20);

		cv::putText(cv_image_in, text, textOrg, fontFace, fontScale, Scalar::all(0), thickness, 5);

		std::stringstream ss1;
		ss1<<"Count of dashes="  << boost::lexical_cast<std::string>(dashs.size()) << " Dist=" << boost::lexical_cast<std::string>(dist).substr(0,6);
		text = ss1.str();

		cv::putText(cv_image_in, text, Point(10,50), fontFace, fontScale, Scalar::all(0), thickness, 5);

		cv::line(cv_image_in, cv::Point(0, intercept), cv::Point(1000, 1000*slope+intercept), CV_RGB(250,0,0));

		PublishImage(result_image_pub_, cv_image_in, "bgr8");
		
		//Detect();
        //cv::imshow("Dashline detector: Dashline Detection",cv_image_in);
        //cv::waitKey(1);


	}
	
	


	void findDashes(float _minContourLengthAllowed)
	{
		std::vector<cv::Point> approxCurve;
		std::vector<Dash> allDashes;

		// For each contour, analyze if it is a parallelepiped likely to be the dash
		for (size_t i=0; i<contours.size(); i++)
		{
			// Approximate to a polygon
			double eps = contours[i].size() * 0.05;
			cv::approxPolyDP(contours[i], approxCurve, eps, true);
			// We interested only in polygons that contains only four points
			if (approxCurve.size() != 4)
				continue;
			// And they have to be convex
			if (!cv::isContourConvex(approxCurve))
				continue;
			// Ensure that the distance between consecutive points is large enough
			float minDist = std::numeric_limits<float>::max();
			for (int j = 0; j < 4; j++)
			{
				cv::Point side = approxCurve[j] - approxCurve[(j+1)%4];
				float squaredSideLength = side.dot(side);
				minDist = std::min(minDist, squaredSideLength);
			}
			// Check that distance is not very small
			if (minDist < _minContourLengthAllowed)
				continue;
			// All tests are passed. Save marker candidate:
			Dash d;
			for (int j = 0; j<4; j++)
				d.points.push_back(
						cv::Point2f(approxCurve[j].x,approxCurve[j].y) );
			// Sort the points in anti-clockwise order
			// Trace a line between the first and second point.
			// If the third point is at the right side, then the points are anticlockwise
			cv::Point v1 = d.points[1] - d.points[0];
			cv::Point v2 = d.points[2] - d.points[0];
			double o = (v1.x * v2.y) - (v1.y * v2.x);
			if (o < 0.0)
				//if the third point is in the left side, then sort in anti-clockwise order
				std::swap(d.points[1], d.points[3]);
			std::swap(d.points[1], d.points[3]);
			d.CalculateParameters();
			allDashes.push_back(d);
		}

		//remove dashes with centers are too close to each other
		for (size_t i=0; i<allDashes.size(); i++)
		{
			if (allDashes[i].isDelete) continue;
			for (size_t j=0; j<allDashes.size(); j++)
			{
				if (allDashes[j].isDelete || i==j) continue;
				cv::Point r = allDashes[i].center - allDashes[j].center;
				double SquareDist = r.dot(r);
				if (SquareDist<10) allDashes[j].isDelete = true;
			}
		}
		dashs.clear();
		for (size_t i=0; i<allDashes.size(); i++)
		{
			if (!allDashes[i].isDelete) dashs.push_back(allDashes[i]);
		}


	}

	void drawAllContours(cv::Mat& Img)
	{
		for (size_t i=0; i<contours.size(); i++)
			cv::drawContours(Img, contours, i, CV_RGB(0,0,250));

	}

	void drawAllDashes(cv::Mat& Img)
	{
		for (size_t i=0; i<dashs.size(); i++)
		{
			dashs[i].Draw(Img);
		}

	}

	void findContours(int minContourPointsAllowed)
	{

		std::vector< std::vector<cv::Point> > allContours;
		cv::findContours(cv_thresholdImg_, allContours, CV_RETR_LIST,
                CV_CHAIN_APPROX_NONE);
		contours.clear();
		for (size_t i=0; i<allContours.size(); i++)
		{
			int contourSize = allContours[i].size();
			if (contourSize > minContourPointsAllowed)
			{
				contours.push_back(allContours[i]);
			}
		}
	}

	void GroupDashesBySlope()
	{
		int n = dashs.size();
		int idGroup=1;

        /*if (lastDirection>-105 && lastDirection<-80){
            //found the left dash
            double minAngle=2*M_PI;
            for (int i=0; i<n; i++)
            {
                double angleOfDash = atan(dashs[i].slope);
                if (angleOfDash<0) angleOfDash = 2*M_PI-angleOfDash;
                if (angleOfDash<minAngle)
                {
                    minAngle = angleOfDash;
                    indVertDash = i;
                }
            }
            ROS_INFO("The most left dash[%d] with angle=%.2f lastdirection=%.2f", indVertDash, atan(dashs[indVertDash].slope), lastDirection);
        } else if (lastDirection<105 && lastDirection>80){
            //found the right dash
            double maxAngle=0;
            for (int i=0; i<n; i++)
            {
                double angleOfDash = atan(dashs[i].slope);
                if (angleOfDash<0) angleOfDash = 2*M_PI-angleOfDash;
                if (angleOfDash>maxAngle)
                {
                    maxAngle = angleOfDash;
                    indVertDash = i;
                }
            }
            ROS_INFO("The most right dash[%d] with angle=%.2f lastdirection=%.2f", indVertDash, atan(dashs[indVertDash].slope),lastDirection);
        } else {
        */
            //found the most vertical dash
            double minDiffAngle = M_PI_2;
            for (int i=0; i<n; i++)
            {
                if ((M_PI_2 - fabs(atan(dashs[i].slope)))<minDiffAngle)
                {
                    minDiffAngle = M_PI_2 - fabs(atan(dashs[i].slope));
                    indVertDash = i;
                }
            }
        //}

		//ROS_INFO("The most vertical dash[%d] with slope=%.2f angle=%.2f", indVertDash, dashs[i].slope, )

		//first group is founded on the most vertical dash
		if (indVertDash>=0) dashs[indVertDash].idGroup=idGroup;
		for (int j=0; j<n; j++)
		{
			if (indVertDash!=j && dashs[j].idGroup==0)
			{
                if ((fabs(fabs(atan(dashs[indVertDash].slope)) - fabs(atan(dashs[j].slope)))<0.2)
                    || (fabs(atan(dashs[indVertDash].slope) - atan(dashs[j].slope))<0.5))
						dashs[j].idGroup=idGroup;
			}

		}
		idGroup++;
		//to group other dashes
		for (int i=0; i<n; i++)
		{
			if (dashs[i].idGroup!=0)
				continue;
			dashs[i].idGroup = idGroup;
			for (int j=0; j<n; j++)
			{
				if (i!=j && dashs[j].idGroup==0)
				{
                    if ((fabs(fabs(atan(dashs[i].slope)) - fabs(atan(dashs[j].slope)))<0.2)
                        || (fabs(atan(dashs[i].slope) - atan(dashs[j].slope))<0.3))
							dashs[j].idGroup=idGroup;
				}


			}
			idGroup++;
		}
	}

	void CalculateLine(double &slope, double &intercept)
	{
	    int n = dashs.size();

	    double sum_x = 0;
	    double sum_y = 0;
	    int count = 0;

	    //calculate linear aproximation for dashes from first group
	    for (int i=0; i<n; i++)
	    {
	    	if (dashs[i].idGroup!=1) continue;
	    	sum_x += dashs[i].center.x;
	    	sum_y += dashs[i].center.y;
	    	count++;
	    }

	    if (count==1 && indVertDash>=0)
	    {
	    	slope = dashs[indVertDash].slope;
	    	intercept = (dashs[indVertDash].center.y - slope*dashs[indVertDash].center.x);
	    }
	    else
	    {
            double avgX = sum_x / count + 0.5; //0.5 чтобы не происходило деление на ноль, когда пунктиры в одну линию
	    	double avgY = sum_y / count;

	    	double numerator = 0.0;
	    	double denominator = 0.0;

	    	for(int i=0; i<n; ++i){
	    		if (dashs[i].idGroup!=1) continue;
	    		numerator += (dashs[i].center.x - avgX) * (dashs[i].center.y - avgY);
	    		denominator += (dashs[i].center.x - avgX) * (dashs[i].center.x - avgX);
	    	}

	    	if (denominator!=0)
	    	{
	    		slope = numerator / denominator;
                if (slope==0) slope = 9999;
	    		intercept = (sum_y - slope*sum_x) / count;
	    	}
	    	else
	    	{
	    		slope = 999;
	    		intercept = 0;
	    	}
	    }

	}


	double CalculateDistToLine(double slope, double intercept, cv::Point2d center)
	{
		cv::Point2d cross;
		cross.x = (center.x/slope+center.y-intercept)/(slope+1/slope);
		cross.y = slope*cross.x+intercept;

		double dist;


		dist = sqrt(pow((cross.x-center.x),2) + pow((cross.y-center.y),2));

		if (center.x<cross.x)
			dist=-dist;

		//ROS_INFO("xc=%.2f yc=%.2f dist=%.2f", cross.x, cross.y, dist);

		return dist;
	}


	void Detect()
	{

		ros::Time prevTime = ros::Time::now();


		cv_image_gray_.convertTo(cv_image_gray_,-1, contrast_gain , brightness);
		resize(cv_image_gray_, cv_image_gray_, Size(0,0), 0.5, 0.5);

		cv::Rect *r;
		cv::Scalar color;

/*		for (uint iface = 0; iface < faces_vec.size(); iface++) {
			r = &faces_vec[iface];
			// Visualization by image display.
			color = cv::Scalar(0,255,0);
			cv::rectangle(cv_image_gray_,
					cv::Point(r->x,r->y),
					cv::Point(r->x+r->width, r->y+r->height), color, 4);

			double x_center = r->x+r->width/2;
			double y_center = r->y+r->height/2;

			visual_system::Cross cross_msg;
			cross_msg.header.stamp = ros::Time::now();
			cross_msg.x = x_center/(cv_image_gray_.size().width/2) - 1; //[-1 1]
			cross_msg.y = y_center/(cv_image_gray_.size().height/2) - 1; //[-1 1]
			cross_msg.type = 1;
			//ROS_INFO("Cross x=%.2f y=%.2f width=%d height=%d xnorm=%f ynorm=%f" , x_center, y_center, cv_image_gray_.size().width, cv_image_gray_.size().height, cross_msg.x, cross_msg.y);
			cross_pub.publish(cross_msg);//пуст_сообщение крест

		}*/

		int fontFace = FONT_HERSHEY_SCRIPT_SIMPLEX;
		double fontScale = 1;
		int thickness = 3;

		std::string text = boost::lexical_cast<std::string>((ros::Time::now() - prevTime).toSec());

		Point textOrg(10,20);

		putText(cv_image_gray_, text, textOrg, fontFace, fontScale, Scalar::all(255), thickness, 8);

		//  ROS_INFO("found %d faces" , faces_vec.size());
		cv::imshow("Dashline detector: Dashline Detection",cv_image_gray_);
		cv::waitKey(1);

	}

	void CaptureVideoFile()
	{

		VideoCapture cap(video_filename); // open the video file
		if(!cap.isOpened()) // check if we succeeded
		{
			ROS_ERROR_STREAM("Video file " << video_filename << " doesn't exist.");
			return;
		}

		ros::Rate loop_rate(10);

		cv::Mat image;

		while (ros::ok())
		{
			cap >> image;
			cvtColor(image, cv_image_gray_, CV_BGR2GRAY);
			Detect();

			ros::spinOnce();
			loop_rate.sleep();

		}

	}

};
// перенести cascade из facedetection faces.cpp

int main(int argc, char** argv)
{
  ros::init(argc, argv, "DashLineDetector");
  DashLineDetect ic;
  ros::spin();
  return 0;
}
