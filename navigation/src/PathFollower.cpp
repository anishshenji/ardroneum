/*
 * PathFollower.cpp
 *
 *  Created on: Nov 27, 2013
 *      Author: alex
 */

#include "PathFollower.h"

PathFollower::PathFollower(ros::Publisher *pub) {
	// TODO Auto-generated constructor stub

	/*    ros::NodeHandle node_private("~/");

    node_private.param("k_trans", K_trans_, 2.0);
    node_private.param("k_rot", K_rot_, 2.0);
    node_private.param("tolerance_trans", tolerance_trans_, 0.1);
    node_private.param("tolerance_rot", tolerance_rot_, 0.2);*/

    K_trans_ = -0.2;
    K_rot_ = -0.5;
    tolerance_trans_ = 0.2;
	tolerance_rot_ = 0.05;
    countMsg=10;

	current_waypoint_ = 0;

	pub_pose_ = pub;

	//test plan
	tf::Pose pose;
	pose.setOrigin(tf::Vector3(1.0,1.0,0));
	tf::Quaternion q;
	q.setRotation(tf::Vector3(0,0,1),M_PI);
	pose.setRotation(q);
	plan_.push_back(pose);

	pose.setOrigin(tf::Vector3(2.0,1.0,0));
	q.setRotation(tf::Vector3(0,0,1),M_PI_2);
	pose.setRotation(q);
	plan_.push_back(pose);

	pose.setOrigin(tf::Vector3(2.0,-1.0,0));
	q.setRotation(tf::Vector3(0,0,1),0);
	pose.setRotation(q);
	plan_.push_back(pose);

	pose.setOrigin(tf::Vector3(1.0,-1.0,0));
	q.setRotation(tf::Vector3(0,0,1),3*M_PI_2);
	pose.setRotation(q);
	plan_.push_back(pose);

	pose.setOrigin(tf::Vector3(0.0,0.0,0));
	q.setRotation(tf::Vector3(0,0,1),M_PI);
	pose.setRotation(q);
	plan_.push_back(pose);


}

PathFollower::~PathFollower() {
	// TODO Auto-generated destructor stub
}

geometry_msgs::Twist PathFollower::diff2D(const tf::Pose& pose1,
		const tf::Pose& pose2) {

	geometry_msgs::Twist res;
	tf::Pose diff = pose2.inverse() * pose1;
	res.linear.x = diff.getOrigin().x();
	res.linear.y = diff.getOrigin().y();
	res.angular.z = tf::getYaw(diff.getRotation());

	return res;
}

bool PathFollower::calculateVelocityCommand(tf::Pose& _robot_pose,
		geometry_msgs::Twist& _cmd_vel) {

	tf::Pose target_pose;
	target_pose = plan_[current_waypoint_];

	geometry_msgs::PoseStamped pose_msg;

	pose_msg.header.stamp = ros::Time::now();

	tf::poseTFToMsg(target_pose, pose_msg.pose);

	pub_pose_->publish(pose_msg);


	//get the difference between the two poses
	geometry_msgs::Twist diff = diff2D( target_pose,  _robot_pose);


	_cmd_vel = limitTwist(diff);

    if (countMsg>=10){

        ROS_INFO("PathFollower: current robot pose %f %f ==> %f", _robot_pose.getOrigin().x(), _robot_pose.getOrigin().y(), tf::getYaw(_robot_pose.getRotation()));
        ROS_INFO("PathFollower: target robot pose %f %f ==> %f", target_pose.getOrigin().x(), target_pose.getOrigin().y(), tf::getYaw(target_pose.getRotation()));

        ROS_INFO("PathFollower: diff %f %f ==> %f", diff.linear.x, diff.linear.y, diff.angular.z);
        ROS_INFO("PathFollower: cmd_vel %f %f ==> %f", _cmd_vel.linear.x, _cmd_vel.linear.y, _cmd_vel.angular.z);
        countMsg=0;
    }
    countMsg++;

	bool in_goal_position = false;
	while(fabs(diff.linear.x) <= tolerance_trans_ &&
			fabs(diff.linear.y) <= tolerance_trans_ &&
			fabs(diff.angular.z) <= tolerance_rot_)
	{
		if(current_waypoint_ < plan_.size() - 1)
		{
			current_waypoint_++;
			target_pose = plan_[current_waypoint_];
			diff = diff2D(target_pose, _robot_pose);
		}
		else
		{
			ROS_INFO("Reached goal: %d", current_waypoint_);
			in_goal_position = true;
			break;
		}
	}

}

void PathFollower::setPlan(
		const std::vector<tf::Pose>& _plan) {

	for(unsigned int i = 0; i < _plan.size(); ++i){
		tf::Pose target_pose=_plan[i];
		plan_.push_back(target_pose);
	}


}

void PathFollower::generateEightPath() {

	plan_.clear();

	float x,y, alpha;
	tf::Pose pose;
	tf::Quaternion q;


	for (float t=M_PI_2;t<2*M_PI+M_PI_2;t+=0.1)
	{

		x = (3*cos(t))/(sin(t)*sin(t)+1);
		y = (3*sin(t)*cos(t))/(sin(t)*sin(t)+1);


		alpha = -atan((3/3)*(3*pow(cos(t),2)-2)/((pow(cos(t),2)+2)*sin(t)));

		if (t>M_PI && t<2*M_PI)
			alpha-=M_PI;

		pose.setOrigin(tf::Vector3(x,y,0));
		q.setRotation(tf::Vector3(0,0,1),alpha);
		pose.setRotation(q);
		plan_.push_back(pose);
	}

}

void PathFollower::generateSimplePath(){
    plan_.clear();

    tf::Pose pose;
    pose.setOrigin(tf::Vector3(2.0,0.0,0));
    tf::Quaternion q;
    q.setRotation(tf::Vector3(0,0,1),-M_PI);
    pose.setRotation(q);
    plan_.push_back(pose);


    pose.setOrigin(tf::Vector3(2.0,0.0,0));
    q.setRotation(tf::Vector3(0,0,1),M_PI_2);
    pose.setRotation(q);
    plan_.push_back(pose);

    pose.setOrigin(tf::Vector3(2.0,1.0,0));
    q.setRotation(tf::Vector3(0,0,1),M_PI_2);
    pose.setRotation(q);
    plan_.push_back(pose);

//	pose.setOrigin(tf::Vector3(2.0,-1.0,0));
//	q.setRotation(tf::Vector3(0,0,1),0);
//	pose.setRotation(q);
//	plan_.push_back(pose);

//	pose.setOrigin(tf::Vector3(1.0,-1.0,0));
//	q.setRotation(tf::Vector3(0,0,1),3*M_PI_2);
//	pose.setRotation(q);
//	plan_.push_back(pose);

//	pose.setOrigin(tf::Vector3(0.0,0.0,0));
//	q.setRotation(tf::Vector3(0,0,1),M_PI);
//	pose.setRotation(q);
//	plan_.push_back(pose);
}

geometry_msgs::Twist PathFollower::limitTwist(
		const geometry_msgs::Twist& twist) {
	geometry_msgs::Twist res = twist;
	res.linear.x *= K_trans_;
	res.linear.y *= K_trans_;
	res.angular.z *= K_rot_;

    if (res.linear.x>0.1) res.linear.x=0.1;
    if (res.linear.x<-0.1) res.linear.x=-0.1;

    if (res.linear.y>0.1) res.linear.y=0.1;
    if (res.linear.y<-0.1) res.linear.y=-0.1;

    if (res.angular.z>0.4) res.angular.z=0.4;
    if (res.angular.z<-0.4) res.angular.z=-0.4;


	//
	//	//make sure to bound things by our velocity limits
	//	double lin_overshoot = sqrt(res.linear.x * res.linear.x + res.linear.y * res.linear.y) / max_vel_lin_;
	//	double lin_undershoot = min_vel_lin_ / sqrt(res.linear.x * res.linear.x + res.linear.y * res.linear.y);
	//	if (lin_overshoot > 1.0)
	//	{
	//		res.linear.x /= lin_overshoot;
	//		res.linear.y /= lin_overshoot;
	//	}
	//
	//	//we only want to enforce a minimum velocity if we're not rotating in place
	//	if(lin_undershoot > 1.0)
	//	{
	//		res.linear.x *= lin_undershoot;
	//		res.linear.y *= lin_undershoot;
	//	}
	//
	//	if (fabs(res.angular.z) > max_vel_th_) res.angular.z = max_vel_th_ * sign(res.angular.z);
	//	if (fabs(res.angular.z) < min_vel_th_) res.angular.z = min_vel_th_ * sign(res.angular.z);

	return res;
}
