#!/usr/bin/env python

import os
import re


def pam2map(filename):
    """Convert Partable anymap(.pgm, .pbm) to 2d [0|1] array"""
    with open(filename, 'r') as f:
        buf = f.read()
    try:
        header, ver, width, height, maxval, byte_array = re.search(
            b"(^P(\d)\s(?:\s*#.*[\r\n])*"
            b"(\d+)\s(?:\s*#.*[\r\n])*"
            b"(\d+)\s(?:\s*#.*[\r\n])*"
            b"(\d+)\s(?:\s*#.*[\r\n]\s)*)"
            b"(.*)", buf).groups()
    except AttributeError:
        raise ValueError("Not a raw PGM file: '%s'" % filename)

    if ver == b'5':
        color_byte_len = 1
    elif ver == b'6':
        color_byte_len = 3
    else:
        color_byte_len = 3

    width, height = int(width), int(height)

    map_data = [[0]*width for _ in range(height)]
    for i in range(height):
        for j in range(width):
            for k in range(color_byte_len):
                if byte_array[i*width*color_byte_len +
                        j*color_byte_len + k] != '\x00':
                    map_data[height - i - 1][j] = 0
                    break
            else:
                map_data[height - i - 1][j] = 1
    print ("Map width=%d height=%d" % (width,height));
    return map_data


def main():
    m = pam2map(os.path.dirname(__file__) + "map150x40.pgm")
    print(m)

if __name__ == "__main__":
    main()
